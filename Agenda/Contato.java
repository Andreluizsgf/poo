public class Contato
{
    String nome;
    int tel;

    public Contato(String nome, int tel){
        this.nome = nome;
        this.tel = tel;
    }

    public int getTel(){
        return this.tel;
    }

    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome){
        this.nome = nome;
    }
}