import java.util.ArrayList;
import java.util.Iterator;

public class Agenda
{
    private ArrayList<Contato> contato;

    public Agenda(){
        this.contato = new ArrayList<Contato>();
    }

    public void addContato(Contato cont){
        this.contato.add(cont);
    }

    public void removeByName(String nome){
        contato.removeIf(obj -> obj.nome == nome);
    }

    public void removeByTel(int tel){
        contato.removeIf(obj -> obj.tel == tel);
    }

    public void printAll(){
        int i = 0;
        for (Contato str : this.contato) {
            i++;
			System.out.println(i + " - " + str.nome + " - " + str.tel);
		}
    }

    public void changeName(int tel, String nome){
        for (Contato str : this.contato) {
            if(str.tel == tel)
                str.setNome(nome);
		}
    }


    public int getSize(){
        return this.contato.size();
    }




}